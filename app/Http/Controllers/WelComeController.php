<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelComeController extends Controller
{
    public function __construct()
    {
        $this->client = new \Google_Client();
        $this->client->setApplicationName("Drive Saver");
        $this->client->setDeveloperKey(env('GDRIVE_KEY'));
        $this->client->setAuthConfig(storage_path('app/credentials.json'));
        $this->client->addScope(\Google_Service_Drive::DRIVE);
        $this->client->setRedirectUri(url('/callback'));
    }

    public function index()
    {
        return redirect($this->client->createAuthUrl());

        return view('welcome');
    }

    public function callback(Request $request)
    {
        $token = $this->client->fetchAccessTokenWithAuthCode($request->input('code'));

        dd($request->all());
    }
}
