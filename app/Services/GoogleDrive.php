<?php

namespace App\Services;

/**
*   Google Drive Class
*/
class GoogleDrive
{
    public function __construct()
    {
        $this->client = new Google_Client();
        $this->client->setApplicationName("Drive Saver");
        $this->client->setDeveloperKey("AIzaSyDicYIWDyv3qI9QwEtBJyDugTXlX_S1V9s");
        $this->client->setAuthConfig(storage_path('client_credentials.json'));
        $this->client->addScope(Google_Service_Drive::DRIVE);
        $this->client->setRedirectUri(url('/callback'));

        return $this->client;
    }
}
